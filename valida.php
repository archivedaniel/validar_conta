<?php
/// <summary>Valida conta corrente
       /// </summary>
       ///<param name="banco">Passar o número (código) do banco</param>
       /// <param name="conta">Passar o número da conta sem o digito</param>
       /// <param name="digito">Passar o digito da conta</param>


$banco = $_POST["banco"];
$agencia = $_POST["agencia"];
$conta = substr($_POST["conta"],0, strlen($_POST["conta"])-1);
$digito = substr($_POST["conta"], -1);

switch ($banco) {
    # Bradesco
    case "247":
        $pesoBradesco = "32765432";

        $soma = 0;
        $resto = 0;
        $Digito = 0;

        $pesoBradesco = substr($pesoBradesco, -strlen($conta));;
        $array_peso = str_split($pesoBradesco);
        $array_conta = str_split($conta);

        foreach($array_conta as $index => $nconta){
            $soma = $soma + (int)$nconta * (int)$array_peso[$index];
        }

        $resto = $soma % 11;

        if ($resto == 0)
        {
            $Digito = 0;
        }
        else
        {
            $Digito = 11 - $resto;
        }
        if ($Digito == 10 || $Digito == 11)
        {
            $digito = 0;
        }

        if ($digito != $Digito)
        {
            echo "Conta inválida";
        }
        else
        {
            echo "Conta válida";
        }
        break;
    # HSBC
    case "399":
        $pesoHsbc = "8923456789";

        $soma = 0;
        $resto = 0;
        $Digito = 0;
        $conta = $agencia.$conta;

        $pesoHsbc = substr($pesoHsbc, -strlen($conta));;
        $array_peso = str_split($pesoHsbc);
        $array_conta = str_split($conta);

        foreach($array_conta as $index => $nconta){
            $soma = $soma + (int)$nconta * (int)$array_peso[$index];
        }

        $resto = $soma % 11;

        if ($resto == 0 || $resto == 10)
        {
            $Digito = 0;
        }
        else
        {
            $Digito = $resto;
        }

        if ($digito != $Digito)
        {
            echo "Conta inválida";
        }
        else
        {
            echo "Conta válida";
        }
        break;
    # Citibank
    case "745":
        $pesoCitiBank = array("11","10","9","8","7","6","5","4","3","2");

        $soma = 0;
        $resto = 0;
        $Digito = 0;
        $conta = $conta;

        // $pesoCitiBank = substr($pesoCitiBank, -strlen($conta));;
        $array_peso = $pesoCitiBank;
        $array_conta = str_split($conta);

        foreach($array_conta as $index => $nconta){
            $soma = $soma + (int)$nconta * (int)$array_peso[$index];
        }

        $resto = $soma % 11;

        if ($resto == 0 || $resto == 10)
        {
            $Digito = 0;
        }
        else
        {
            $Digito = 11 - $resto;
        }
        if ($digito != $Digito)
        {
            echo "Conta inválida";
        }
        else
        {
            echo "Conta válida";
        }
        break;
    # Caixa
    case "104":
        $pesoCaixa = "9876543298765432";

        $soma = 0;
        $resto = 0;
        $Digito = 0;
        $conta = $agencia.$conta;

        $pesoCaixa = substr($pesoCaixa, -strlen($conta));;
        $array_peso = str_split($pesoCaixa);
        $array_conta = str_split($conta);
        foreach($array_conta as $index => $nconta){
            $soma = $soma + (int)$nconta * (int)$array_peso[$index];
        }

        $soma = $soma * 10;
        $subtrair = (int)($soma / 11) * 11;
        $resto = $soma - $subtrair;
        $Digito = ($resto == 10 ? 0 : $resto);

        if ($digito != $Digito)
        {
            echo "Conta inválida";
        }
        else
        {
            echo "Conta válida";
        }
        break;
    #Itaú
    case "341":
        $pesoItau = "212121212";
        $soma = 0;
        $resto = 0;
        $Digito = 0;

        $conta = $agencia.$conta;
        $array_conta = str_split($conta);

        $array_peso = str_split($pesoItau);

        foreach($array_conta as $index => $nconta){
            $soma_digito = (int)$nconta * (int)$array_peso[$index];

            if($soma_digito > 9){
              $array_digito = str_split((string)$soma_digito);
              $soma_digito = 0;
              foreach ($array_digito as $d) {
                  $soma_digito = $soma_digito + (int)$d;
              }
            }

            $soma = $soma + $soma_digito;
        }

        $resto = $soma % 10;
        if($resto == 0){
          $Digito = 0;
        } else {
          $Digito = 10 - $resto;
        }

        if ($digito != $Digito)
        {
            echo "Conta inválida";
        }
        else
        {
            echo "Conta válida";
        }
        break;
    #Santander
    case "033":
        $pesoSantander = "973197131973";
        $soma = 0;
        $resto = 0;
        $Digito = 0;

        $conta = $agencia.$conta;
        $array_conta = str_split($conta);
        $array_peso = str_split($pesoSantander);

        foreach($array_conta as $index => $nconta){
            $soma_digito = (int)$nconta * (int)$array_peso[$index];

            if($soma_digito > 9){
              $soma_digito = (int)str_split((string)$soma_digito)[1];
            }
            $soma = $soma + $soma_digito;
        }

        $resto = $soma % 10;
        if($resto == 0){
          $Digito = 0;
        } else {
          $Digito = 10 - $resto;
        }

        if ($digito != $Digito)
        {
            echo "Conta inválida";
        }
        else
        {
            echo "Conta válida";
        }
        break;
    # Banco do Brasil
    case "001":
        $pesoBB = "98765432";
        $soma = 0;
        $resto = 0;
        $Digito = 0;

        $pesoBB = substr($pesoBB, -strlen($conta));
        $array_peso = str_split($pesoBB);
        $array_conta = str_split($conta);

        foreach($array_conta as $index => $nconta){
            $soma = $soma + (int)$nconta * (int)$array_peso[$index];
        }

        $resto = $soma % 11;
        $Digito = 11 - $resto;
        if($Digito == 11){
          $Digito = 0;
        }else if($Digito == 10){
          $Digito = 'x';
        }

        if ($digito != $Digito)
        {
            echo "Conta Inválida";
        }
        else
        {
            echo "Conta Válida!";
        }
        break;
    }
?>
