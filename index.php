<!DOCTYPE html>
<?php
$contas = [["001","Banco do Brasil","0952","2531348"],
["001","Banco do Brasil","1151","1478982"],
["001","Banco do Brasil","0547","1106630x"],
["247","Bradesco","0532","00105856"],
["247","Bradesco","5236","17293397"],
["247","Bradesco","2631","08828008"],
["341","Itau","1968","062933"],
["341","Itau","0764","511697"],
["341","Itau","3727","529616"],
["104","Caixa","2946","001849619448"],
["104","Caixa","4684","011848743098"],
["104","Caixa","1315","013775796619"],
["745","Citibank","0066","94611156435"],
["745","Citibank","0079","03796710247"],
["745","Citibank","0014","65027072376"],
["399","HSBC","0209","4717725"],
["399","HSBC","0463","2497591"],
["399","HSBC","1195","5204572"],
["033","Santander","0153","033403930"],
["033","Santander","0792","469336085"],
["033","Santander","0026","035003115"]];
?>

<html>
<head>
<title>Validar Conta Bancária</title>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
</head>
<body>
    <table border="1px">
      <tr>
        <th>Banco</th>
        <th>Agencia</th>
        <th>Conta</th>
        <th>Selecionar</th>
      </tr>
      <?php foreach($contas as $conta) { ?>
        <tr class="row">
        <td class="banco"><?php echo $conta[1];?></td>
        <td class="agencia"><?php echo $conta[2];?></td>
        <td class="conta"><?php echo $conta[3];?></td>
        <td><a class="selecionar" href="#" onclick="selecionar($(this),'<?php echo $conta[0] ?>')">Selecionar</a></td>
        </tr>
      <?php } ?>
    </table>

    Banco:
    <select id='banco' name='banco'>
        <option value="001">Banco do Brasil</option>
        <option value="247">Bradesco</option>
        <option value="341">Itaú</option>
        <option value="104">Caixa</option>
        <option value="033">Santander</option>
        <option value="745">CitiBank</option>
        <option value="399">HSBC</option>
    </select>
    </br>
    Agencia: <input type='text' id='agencia' name='agencia'/></br>
    Conta: <input type='text' id='conta' name='conta'/></br>

    <div id="resultado">
    </div>


    <a href="#" id="validar">Validar</a>
</body>
<script>
$('#validar').click(function(){
    $.post('valida.php', { banco: $("#banco").val(), agencia: $("#agencia").val(), conta: $("#conta").val()  } ,function(data){
        $('#resultado').html(data);
    })
});

function selecionar(t, banco){
  var pai = t.parent().parent();
  $("#banco").val(banco);
  $("#agencia").val(pai.find('.agencia').text());
  $("#conta").val(pai.find('.conta').text());
}
</script>
